package com.api.crud.domain.usecase;

import com.api.crud.domain.model.*;
import org.springframework.stereotype.Service;

@Service
public class DeleteProductUseCase {
    private final ProductRepository repository;

    public DeleteProductUseCase(ProductRepository repository) {
        this.repository = repository;
    }

    public void execute(int id) throws ProductNotFoundException {
        Product product = repository.findById(id).orElseThrow(()-> new ProductNotFoundException("Product no encontrado"));
        repository.delete(product);
    }
}
