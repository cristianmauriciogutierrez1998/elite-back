package com.api.crud.domain.usecase;

import com.api.crud.domain.model.*;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class CreateProductUseCase {
    private final ProductRepository repository;

    public CreateProductUseCase(ProductRepository repository) {
        this.repository = repository;
    }
    public Product execute(Product producto) {
        return repository.save(producto);
    }
}
