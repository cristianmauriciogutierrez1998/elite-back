package com.api.crud.domain.usecase;

import com.api.crud.domain.model.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GetProductUseCase {
    private final ProductRepository repository;

    public GetProductUseCase(ProductRepository repository) {
        this.repository = repository;
    }

    public List<Product> getAllProducts() {
        return repository.findAll();
    }

    public Product getProductById(int id) {
        Optional<Product> optionalProduct = repository.findById(id);
        return optionalProduct.orElseThrow(()-> new ProductNotFoundException("Producto no encontrado"));
    }
}
