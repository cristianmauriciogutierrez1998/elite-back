package com.api.crud.domain.usecase;

import com.api.crud.domain.model.*;
import org.springframework.stereotype.Service;

@Service
public class UpdateProductUseCase {
    private final ProductRepository repository;

    public UpdateProductUseCase(ProductRepository repository) {
        this.repository = repository;
    }

    public Product execute(int id, Product producto) throws ProductNotFoundException {
        Product existingProduct = repository.findById(id).orElseThrow(()-> new ProductNotFoundException("Producto no encontrado"));

        existingProduct.setASIN(existingProduct.getASIN());
        existingProduct.setSKU(existingProduct.getSKU());
        existingProduct.setNombre(existingProduct.getNombre());
        existingProduct.setPrecio(existingProduct.getPrecio());
        existingProduct.setDescripcion(existingProduct.getDescripcion());
        existingProduct.setManual_usuario(existingProduct.getManual_usuario());
        existingProduct.setCantidad_disponible(existingProduct.getCantidad_disponible());
        existingProduct.setImagen(existingProduct.getImagen());
        repository.update(id, existingProduct);
        return existingProduct;
    }
}
