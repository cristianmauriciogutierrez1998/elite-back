package com.api.crud.domain.model;

import com.api.crud.application.ProductDto;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

public interface ProductRepository {
    List<Product> findAll();
    Optional<Product> findById(int id);
    Product save(Product product);
    void update(int id, Product product);
    void delete(Product product);
}
