package com.api.crud.domain.model;

import com.api.crud.infrastructure.persistence.ProductEntity;

import java.util.List;
import java.util.stream.Collector;

public class Product {
    private int id;
    private String ASIN;
    private String SKU;
    private String nombre;
    private double precio;
    private String descripcion;
    private String manual_usuario;
    private int cantidad_disponible;
    private String imagen;

    // Constructores
    public Product(Product product) {
        this.ASIN = product.getASIN();
        this.SKU = product.getSKU();
        this.nombre = product.getNombre();
        this.precio = product.getPrecio();
        this.descripcion = product.getDescripcion();
        this.manual_usuario = product.getManual_usuario();
        this.cantidad_disponible = product.getCantidad_disponible();
        this.imagen = product.getImagen();
    }

    public Product(int id, String asin, String sku, String nombre, double precio, String descripcion, String manual_usuario, int cantidad_disponible, String imagen) {
        this.ASIN = asin;
        this.SKU = sku;
        this.nombre = nombre;
        this.precio = precio;
        this.descripcion = descripcion;
        this.manual_usuario = manual_usuario;
        this.cantidad_disponible = cantidad_disponible;
        this.imagen = imagen;
    }

    public Product() {
    }

    public Product(ProductEntity productEntity) {
        this.ASIN = productEntity.getASIN();
        this.SKU = productEntity.getSKU();
        this.nombre = productEntity.getNombre();
        this.precio = productEntity.getPrecio();
        this.descripcion = productEntity.getDescripcion();
        this.manual_usuario = productEntity.getManual_usuario();
        this.cantidad_disponible = productEntity.getCantidad_disponible();
        this.imagen = productEntity.getImagen();
    }

    public Product(String asin, String sku, String nombre, Double precio, String descripcion, String manual_usuario, int cantidad_disponible, String imagen) {
        this.ASIN = asin;
        this.SKU = sku;
        this.nombre = nombre;
        this.precio = precio;
        this.descripcion = descripcion;
        this.manual_usuario = manual_usuario;
        this.cantidad_disponible = cantidad_disponible;
        this.imagen = imagen;
    }

    //getters y setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getASIN() {
        return ASIN;
    }

    public void setASIN(String ASIN) {
        this.ASIN = ASIN;
    }

    public String getSKU() {
        return SKU;
    }

    public void setSKU(String SKU) {
        this.SKU = SKU;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getManual_usuario() {
        return manual_usuario;
    }

    public void setManual_usuario(String manual_usuario) {
        this.manual_usuario = manual_usuario;
    }

    public int getCantidad_disponible() {
        return cantidad_disponible;
    }

    public void setCantidad_disponible(int cantidad_disponible) {
        this.cantidad_disponible = cantidad_disponible;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

}
