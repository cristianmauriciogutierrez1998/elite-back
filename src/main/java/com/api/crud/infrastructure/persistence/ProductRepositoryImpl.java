package com.api.crud.infrastructure.persistence;

import com.api.crud.domain.model.*;


import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class ProductRepositoryImpl implements ProductRepository{

    private final ProductRepositoryJpa productRepositoryJpa;

    public ProductRepositoryImpl(ProductRepositoryJpa productRepositoryJpa) {
        this.productRepositoryJpa = productRepositoryJpa;
    }

    @Override
    public List<Product> findAll() {
        List<ProductEntity> entities = productRepositoryJpa.findAll();
        return entities.stream().map(entity -> new Product(entity.getId(), entity.getASIN(), entity.getSKU(), entity.getNombre(), entity.getPrecio(), entity.getDescripcion(), entity.getManual_usuario(), entity.getCantidad_disponible(), entity.getImagen())).collect(Collectors.toList());
    }

    @Override
    public Optional<Product> findById(int id) {
        Optional<ProductEntity> optionalProductEntity = productRepositoryJpa.findById(id);
        return optionalProductEntity.map(entity-> new Product(entity.getId(), entity.getASIN(), entity.getSKU(), entity.getNombre(), entity.getPrecio(), entity.getDescripcion(), entity.getManual_usuario(), entity.getCantidad_disponible(), entity.getImagen()));
    }

    @Override
    public Product save(Product product) {
        ProductEntity productEntity = new ProductEntity();
        productEntity.setASIN(product.getASIN());
        productEntity.setSKU(product.getSKU());
        productEntity.setNombre(product.getNombre());
        productEntity.setPrecio(product.getPrecio());
        productEntity.setDescripcion(product.getDescripcion());
        productEntity.setManual_usuario(product.getManual_usuario());
        productEntity.setCantidad_disponible(product.getCantidad_disponible());
        productEntity.setImagen(product.getImagen());
        ProductEntity savedProductEntity = productRepositoryJpa.save(productEntity);
        return new Product(savedProductEntity.getASIN(), savedProductEntity.getSKU(), savedProductEntity.getNombre(), savedProductEntity.getPrecio(), savedProductEntity.getDescripcion(), savedProductEntity.getManual_usuario(), savedProductEntity.getCantidad_disponible(), savedProductEntity.getImagen());
    }

    @Override
    public void update(int id, Product product) {
        Optional<ProductEntity> optionalProductEntity = productRepositoryJpa.findById(id);
        if (optionalProductEntity.isPresent()){
            ProductEntity productEntity = optionalProductEntity.get();
            productEntity.setASIN(product.getASIN());
            productEntity.setSKU(product.getSKU());
            productEntity.setNombre(product.getNombre());
            productEntity.setPrecio(product.getPrecio());
            productEntity.setDescripcion(product.getDescripcion());
            productEntity.setManual_usuario(product.getManual_usuario());
            productEntity.setCantidad_disponible(product.getCantidad_disponible());
            productEntity.setImagen(product.getImagen());
            productRepositoryJpa.save(productEntity);
        }
    }

    public void delete(Product product) {
    }
}
