package com.api.crud.infrastructure.persistence;


import com.api.crud.domain.model.Product;
import com.api.crud.domain.model.ProductRepository;
import com.api.crud.domain.usecase.DeleteProductUseCase;
import com.api.crud.infrastructure.persistence.ProductEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ProductRepositoryJpa extends JpaRepository<ProductEntity, Integer> {
}
