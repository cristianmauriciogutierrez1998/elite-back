package com.api.crud.infrastructure.persistence;

import com.api.crud.domain.model.Product;
import jakarta.persistence.*;

@Entity
@Table(name="productos")
public class ProductEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String ASIN;

    @Column(nullable = false)
    private String SKU;

    @Column(nullable = false)
    private String nombre;

    @Column(nullable = false)
    private Double precio;

    private String descripcion;

    private String manual_usuario;

    @Column(nullable = false)
    private int cantidad_disponible;

    private String imagen;

    // Constructor vacío para JPA

    public ProductEntity(Integer id, String asin, String sku, String nombre, Double precio, String descripcion, String manual_usuario, int cantidad_disponible, String imagen) {
        this.id = id;
        this.ASIN = asin;
        this.SKU = sku;
        this.nombre = nombre;
        this.precio = precio;
        this.descripcion = descripcion;
        this.manual_usuario = manual_usuario;
        this.cantidad_disponible = cantidad_disponible;
        this.imagen = imagen;
    }

    public ProductEntity() {
    }

    //getters y setters
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getASIN() {
        return ASIN;
    }

    public void setASIN(String ASIN) {
        this.ASIN = ASIN;
    }

    public String getSKU() {
        return SKU;
    }

    public void setSKU(String SKU) {
        this.SKU = SKU;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getManual_usuario() {
        return manual_usuario;
    }

    public void setManual_usuario(String manual_usuario) {
        this.manual_usuario = manual_usuario;
    }

    public int getCantidad_disponible() {
        return cantidad_disponible;
    }

    public void setCantidad_disponible(int cantidad_disponible) {
        this.cantidad_disponible = cantidad_disponible;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
}
