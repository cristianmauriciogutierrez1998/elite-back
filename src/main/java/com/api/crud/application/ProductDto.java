package com.api.crud.application;

import com.api.crud.domain.model.Product;

import java.util.Optional;

public class ProductDto {

    private int id;
    private String ASIN;
    private String SKU;
    private String nombre;
    private double precio;
    private String descripcion;
    private String manual_usuario;
    private int cantidad_disponible;
    private String imagen;

    public ProductDto(){
    }
    public ProductDto(Product product) {
        this.ASIN = product.getASIN();
        this.SKU = product.getSKU();
        this.nombre = product.getNombre();
        this.precio = product.getPrecio();
        this.descripcion = product.getDescripcion();
        this.manual_usuario = product.getManual_usuario();
        this.cantidad_disponible = product.getCantidad_disponible();
        this.imagen = product.getImagen();
    }
    public Product toProduct() {
        Product product = new Product();
        product.setNombre(this.getNombre());
        product.setASIN(this.getASIN());
        product.setSKU(this.getSKU());
        product.setPrecio(this.getPrecio());
        product.setDescripcion(this.getDescripcion());
        product.setManual_usuario(this.getManual_usuario());
        product.setCantidad_disponible(this.getCantidad_disponible());
        product.setImagen(this.getImagen());
        return product;
    }

    public ProductDto(int id, String ASIN, String SKU, String nombre, double precio, String descripcion, String manual_usuario, int cantidad_disponible, String imagen) {
        this.id = id;
        this.ASIN = ASIN;
        this.SKU = SKU;
        this.nombre = nombre;
        this.precio = precio;
        this.descripcion = descripcion;
        this.manual_usuario = manual_usuario;
        this.cantidad_disponible = cantidad_disponible;
        this.imagen = imagen;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getASIN() {
        return ASIN;
    }

    public void setASIN(String ASIN) {
        this.ASIN = ASIN;
    }

    public String getSKU() {
        return SKU;
    }

    public void setSKU(String SKU) {
        this.SKU = SKU;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getManual_usuario() {
        return manual_usuario;
    }

    public void setManual_usuario(String manual_usuario) {
        this.manual_usuario = manual_usuario;
    }

    public int getCantidad_disponible() {
        return cantidad_disponible;
    }

    public void setCantidad_disponible(int cantidad_disponible) {
        this.cantidad_disponible = cantidad_disponible;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
}
