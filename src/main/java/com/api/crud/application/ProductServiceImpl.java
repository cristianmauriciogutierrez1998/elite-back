package com.api.crud.application;


import com.api.crud.domain.model.*;
import com.api.crud.domain.usecase.CreateProductUseCase;
import com.api.crud.domain.usecase.DeleteProductUseCase;
import com.api.crud.domain.usecase.GetProductUseCase;
import com.api.crud.domain.usecase.UpdateProductUseCase;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

    private final CreateProductUseCase createProductUseCase;
    private final GetProductUseCase getProductUseCase;
    private final UpdateProductUseCase updateProductUseCase;
    private final DeleteProductUseCase deleteProductUseCase;

    public ProductServiceImpl(CreateProductUseCase createProductUseCase, GetProductUseCase getProductUseCase, UpdateProductUseCase updateProductUseCase, DeleteProductUseCase deleteProductUseCase) {
        this.createProductUseCase = createProductUseCase;
        this.getProductUseCase = getProductUseCase;
        this.updateProductUseCase = updateProductUseCase;
        this.deleteProductUseCase = deleteProductUseCase;
    }

    @Override
    public List<ProductDto> getAllProducts() {
        List<Product> products = getProductUseCase.getAllProducts();
        return products.stream().map(ProductDto::new).collect(Collectors.toList());
    }

    @Override
    public ProductDto getProduct(int id) {
        Product product = getProductUseCase.getProductById(id);
        return new ProductDto(product);
    }

    @Override
    public ProductDto createProduct(ProductDto productDto) {
        Product product = createProductUseCase.execute(productDto.toProduct());
        return new ProductDto(product);
    }

    @Override
    public ProductDto updateProduct(int id, ProductDto productDto) {
        Product product = updateProductUseCase.execute(id, productDto.toProduct());
        return new ProductDto(product);
    }
    @Override
    public void deleteProduct(int id){
        deleteProductUseCase.execute(id);
    }
}
