package com.api.crud.application;


import com.api.crud.infrastructure.web.ProductRequest;

import java.util.List;

public interface  ProductService {
    List<ProductDto> getAllProducts();
    ProductDto getProduct(int id);
    ProductDto createProduct(ProductDto productDto);
    ProductDto updateProduct(int id, ProductDto productDto);
    void deleteProduct(int id);

}
